"use strict";
window.wrappedJSObject.Reflect.parse=cloneInto(
	function parse(sourceCode, options={}){
		return new window.Promise(function(resolve, reject){
			return browser.runtime.sendMessage({sourceCode, options}).then((res)=>{
				let unwraping=window.Object();
				unwraping.wrappedJSObject.res=cloneInto(res, window, {cloneFunctions: false});;
				resolve(unwraping.wrappedJSObject.res);
			});
		});
	}, window, {cloneFunctions: true}
);