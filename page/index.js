"use strict";
let jsEl=document.getElementById("JS");
let astEl=document.getElementById("AST");

if(!Reflect.parse){
	console.error(Reflect.parse);
	jsEl.value="You need the extensions to test this API";
}
function updateAST(evt){
	return Reflect.parse(jsEl.value).then(res => {console.log(res);astEl.value=JSON.stringify(res, null, "\t");}, err =>{astEl.value=err.toString();});
}
updateAST();
jsEl.addEventListener("change", updateAST, false);
jsEl.addEventListener("input", updateAST, false);
