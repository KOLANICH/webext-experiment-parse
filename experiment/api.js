"use strict";

Components.utils.import("resource://gre/modules/reflect.jsm");
//ChromeUtils.import("resource:///modules/SitePermissions.jsm");

class ReflectParseAPI extends ExtensionAPI {
	getAPI() {
		return {
			experiments : {
				parse : {
					async parse(sourceCode, loc=true, source="", line=1) { // you must double default args in signature
						/*if(SitePermissions.get(gBrowser.currentURI, "reflect-parse") == 0){
							SitePermissions.set(gBrowser.currentURI, "reflect-parse", SitePermissions.ASK)
						)*/
						//console.log(sourceCode, loc, source, line*);
						return new Promise((resolve, reject)=>{
							try{
								resolve(Reflect.parse(sourceCode, {loc, source, line}));
							}
							catch(ex){
								reject(ex);
							}
						});
					},
				},
			},
		};
	}
};

var identifierOfVariable1;
identifierOfVariable1=ReflectParseAPI; //let, const, class, export or without var don't work. 
